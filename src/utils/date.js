export const getDayFromDate = (date) => {
  let date_str = date.split(" ")[0];
  let d = new Date(date_str);
  let weekday = new Array(7);
  weekday[0] = "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

  let n = weekday[d.getDay()];
  return n;
};

export const formatDate = (date) => {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
};

export const getDateAsString = (date) => {
  return date.split(" ")[0];
};

export const getPrevDay = (date) => {
  let date_str = date.split(" ")[0];
  let day = new Date(date_str);
  let prevDay = new Date(day);
  prevDay.setDate(day.getDate() - 1);
  return formatDate(prevDay);
};

export const getNextDay = (date) => {
  let date_str = date.split(" ")[0];
  let day = new Date(date_str);
  let nextDay = new Date(day);
  nextDay.setDate(day.getDate() + 1);
  return formatDate(nextDay);
};

export default { getDayFromDate, getPrevDay, getNextDay, formatDate };
