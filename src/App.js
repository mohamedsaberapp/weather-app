import React, { useState } from "react";
import { Container, Spinner } from "react-bootstrap";
import SearchBar from "./components/SearchBar/SearchBar";
import Forecast from "./components/Forecast/Forecast";
import "./App.scss";

function App() {
  const [forecastData, setForecastData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const handleError = (data) => {
    setForecastData(null);
    setError(data.message);
  };
  const getForecastData = (data) => {
    setError(null);
    setForecastData(data);
  };

  const isFetching = (boolean) => {
    setIsLoading(boolean);
  };

  return (
    <Container fluid className="wrapper">
      <Container className="wrapper__widget">
        <SearchBar
          onError={handleError}
          isFetching={isFetching}
          onDataRetrieve={getForecastData}
        />
        <div className="wrapper__widget__body">
          {isLoading ? (
            <div className="wrapper__spinner d-flex">
              <Spinner animation="grow" variant="primary" />
            </div>
          ) : (
            <Forecast error={error} forecastData={forecastData} />
          )}
        </div>
      </Container>
    </Container>
  );
}

export default App;
