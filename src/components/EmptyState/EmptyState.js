import React from "react";
import "./Styles.scss";

const EmptyState = ({ message }) => {
  return <div className="empty-wrapper">{message}</div>;
};

export default EmptyState;
