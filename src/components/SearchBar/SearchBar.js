import Axios from "axios";
import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { base, key } from "../../utils/apiConfig";
import "./Styles.scss";

const SearchBar = ({ onDataRetrieve, isFetching, onError }) => {
  const [query, setQuery] = useState("");

  const getForecastByCityName = async () => {
    isFetching(true);
    let url = base + `forecast.json?key=${key}&q=${query}&days=3`;
    Axios.get(url)
      .then((response) => {
        onDataRetrieve(response.data);
        isFetching(false);
      })
      .catch((error) => {
        onError({ message: error.response.data.error.message });
        isFetching(false);
      });
  };

  return (
    <div className="search-wrapper">
      <input
        className="search-wrapper__input"
        type="text"
        value={query}
        placeholder="Enter city name..."
        onChange={(e) => setQuery(e.target.value)}
      />
      <Button disabled={!query} onClick={getForecastByCityName}>
        Search
      </Button>
    </div>
  );
};

export default SearchBar;
