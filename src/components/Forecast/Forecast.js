import React, { useState } from "react";
import { Row, Col } from "react-bootstrap";
import EmptyState from "../EmptyState/EmptyState";
import MiniForecast from "../MiniForecast/MiniForecast";
import {
  getDayFromDate,
  getNextDay,
  getPrevDay,
  formatDate,
} from "../../utils/date";
import { WiHumidity, WiStrongWind } from "react-icons/wi";
import "./Styles.scss";

const Forecast = ({ forecastData, error }) => {
  const [state, setState] = useState(forecastData);

  const handleSelectDay = (data) => {
    return setState(data);
  };

  return (
    <div className="forecast-wrapper">
      {state ? (
        <>
          <Row noGutters>
            <Col xs={12} lg={6}>
              <div className="forecast-wrapper__current-temp">
                <img
                  src={state.current?.condition?.icon}
                  alt="Current Forecast"
                />
                <div className="forecast-wrapper__condition-text">
                  {getDayFromDate(
                    state.forecast?.forecastday[0]?.date || state.current?.date
                  )}{" "}
                  | {state.current?.condition?.text}
                </div>
                <div className="forecast-wrapper__temperature">
                  {state.current?.temp_c} &#8451;
                </div>
                <div className="forecast-wrapper__location-name">
                  {state.location?.name}
                </div>
                <div className="forecast-wrapper__location-country">
                  {state.location?.country}
                </div>
              </div>
            </Col>
            <Col xs={12} lg={6}>
              <div className="forecast-wrapper__wind">
                <div className="forecast-wrapper__wind__item">
                  <div className="icon">
                    <WiHumidity />
                  </div>
                  <div className="data">
                    <span>Humidity</span>
                    <h3>{state.current?.humidity} %</h3>
                  </div>
                </div>
                <div className="forecast-wrapper__wind__item">
                  <div className="icon">
                    <WiStrongWind />
                  </div>
                  <div className="data">
                    <span>Wind Speed</span>
                    <h3>{state.current?.wind_kph} km/h</h3>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col>
              <MiniForecast
                date={getPrevDay(formatDate(new Date()))}
                city={state.location?.name}
                onSelect={handleSelectDay}
              />
            </Col>
            <Col>
              <MiniForecast
                date={formatDate(new Date())}
                city={state.location?.name}
                onSelect={handleSelectDay}
              />
            </Col>
            <Col>
              <MiniForecast
                date={getNextDay(formatDate(new Date()))}
                city={state.location?.name}
                onSelect={handleSelectDay}
              />
            </Col>
          </Row>
        </>
      ) : (
        <Row noGutters>
          <Col>
            <EmptyState message={error ? error : "Type city name and search"} />
          </Col>
        </Row>
      )}
    </div>
  );
};

export default Forecast;
