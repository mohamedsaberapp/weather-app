import Axios from "axios";
import React, { useState, useEffect } from "react";
import { base, key } from "../../utils/apiConfig";
import { getDayFromDate } from "../../utils/date";
import "./Styles.scss";

const MiniForecast = ({ date, city, onSelect }) => {
  const [data, setData] = useState(null);

  const selectDay = () => {
    const mappedData = {
      current: {
        date: data?.forecast?.forecastday[0].date,
        condition: data?.forecast?.forecastday[0]?.day?.condition,
        humidity: data?.forecast?.forecastday[0]?.day?.avghumidity,
        wind_kph: data?.forecast?.forecastday[0]?.day?.maxwind_kph,
        temp_c: data?.forecast?.forecastday[0]?.day?.avgtemp_c,
      },
      location: data?.location,
    };
    onSelect(mappedData);
  };

  const getForecastByDate = () => {
    let url = base + `history.json?key=${key}&q=${city}&dt=${date}`;
    Axios.get(url)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error?.response?.data?.error?.message);
      });
  };

  useEffect(() => {
    getForecastByDate();
    //eslint-disable-next-line
  }, []);

  return (
    data && (
      <div className="mini-wrapper" onClick={selectDay}>
        <img
          src={data?.forecast?.forecastday[0]?.day.condition?.icon}
          alt="Current Forecast"
        />
        <div className="mini-wrapper__temp">
          {data?.forecast?.forecastday[0]?.day?.avgtemp_c} &#8451;
        </div>
        <div className="mini-wrapper__condition-text">
          {data?.forecast?.forecastday[0]?.day?.condition?.text}
        </div>
        <div className="mini-wrapper__date">
          {getDayFromDate(data?.forecast?.forecastday[0]?.date)}
        </div>
      </div>
    )
  );
};

export default MiniForecast;
